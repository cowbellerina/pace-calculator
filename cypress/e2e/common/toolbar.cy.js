describe('Toolbar', () => {
  describe('URL params', () => {
    it('retrieves the value from URL', () => {
      cy.visit('/?unit=mi')
      cy.get('[data-testid="unit"]').should('have.value', 'mi')
    })
    it('accepts only valid unit values', () => {
      cy.visit('/?unit=abc')
      cy.get('[data-testid="unit"]').should('have.value', 'km') // default
    })
  })

  describe('Local Storage', () => {
    beforeEach(() => {
      cy.visit('/')

      const state = {settings: {unit: 'mi'}}
      window.localStorage.setItem('paceCalculator', JSON.stringify(state))
    })

    it('should fetch settings from Local Storage if available', () => {
      const state = JSON.parse(window.localStorage.getItem('paceCalculator'))
      expect(state.settings.unit).to.eq('mi')

      cy.reload()

      cy.get('[data-testid="unit"]').should('have.value', 'mi')
    })

    it('should save settings to Local Storage on change', () => {
      cy.get('[data-testid="unit"]').select('km').then(() => {
        const state = JSON.parse(window.localStorage.getItem('paceCalculator'))
        expect(state.settings.unit).to.eq('km')
      })
    })
  })
})
