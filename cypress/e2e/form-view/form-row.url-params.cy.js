describe('Form Row - URL params', () => {
  describe('Kitchen sink', () => {
    it('parses all URL params', () => {
      cy.visit('/?distance=12.3&distance=23.4&time=1h23m45s&time=2h34m56s&unit=mi')

      cy.get('[data-testid="unit"]').should('have.value', 'mi')

      cy.get('[data-testid="form-row-0"] [data-testid="distance"]').should('have.value', '12.3')
      cy.get('[data-testid="form-row-0"] [data-testid="distance-list"]').should('have.value', 'custom')
      cy.get('[data-testid="form-row-0"] [data-testid="pace-minutes"]').should('have.value', '6')
      cy.get('[data-testid="form-row-0"] [data-testid="pace-seconds"]').should('have.value', '48')
      cy.get('[data-testid="form-row-0"] [data-testid="time-hours"]').should('have.value', '1')
      cy.get('[data-testid="form-row-0"] [data-testid="time-minutes"]').should('have.value', '23')
      cy.get('[data-testid="form-row-0"] [data-testid="time-seconds"]').should('have.value', '45')

      cy.get('[data-testid="form-row-1"] [data-testid="distance"]').should('have.value', '23.4')
      cy.get('[data-testid="form-row-1"] [data-testid="distance-list"]').should('have.value', 'custom')
      cy.get('[data-testid="form-row-1"] [data-testid="pace-minutes"]').should('have.value', '6')
      cy.get('[data-testid="form-row-1"] [data-testid="pace-seconds"]').should('have.value', '37')
      cy.get('[data-testid="form-row-1"] [data-testid="time-hours"]').should('have.value', '2')
      cy.get('[data-testid="form-row-1"] [data-testid="time-minutes"]').should('have.value', '34')
      cy.get('[data-testid="form-row-1"] [data-testid="time-seconds"]').should('have.value', '56')
    })

    it('updates URL on change', () => {
      cy.visit('/')

      cy.get('[data-testid="distance"]').type('12.3')
      cy.get('[data-testid="time-hours"]').type('1')
      cy.get('[data-testid="time-minutes"]').type('23')
      cy.get('[data-testid="time-seconds"]').type('45')

      cy.url().should('include', 'distance=12.3')
      cy.url().should('include', 'time=1h23m45s')
    })

    it.skip('should not pollute history', () => {
      cy.visit('/')

      cy.get('[data-testid="distance"]').clear().type('10')
      cy.url().should('include', 'distance=10')

      cy.reload()
      cy.url().should('include', 'distance=10')

      cy.get('[data-testid="distance"]').clear().type('11')
      cy.url().should('include', 'distance=11')
      cy.get('[data-testid="distance"]').clear().type('12')
      cy.url().should('include', 'distance=12')

      cy.reload()
      // Yields the following error:
      // Blocked a frame with origin "https://localhost:3000" from accessing a cross-origin frame.
      cy.go('back')

      cy.url().should('not.include', 'distance')
      cy.url().should('not.include', 'time')
    })
  })

  describe('Distance', () => {
    it('retrieves the value from URL', () => {
      cy.visit('/?distance=20.75')
      cy.get('[data-testid="distance"]').should('have.value', '20.8')
    })
    it('accepts only numbers', () => {
      cy.visit('/?distance=abc')
      cy.get('[data-testid="distance"]').should('have.value', '0') // default
    })
    it('requires distance to be larger than 0', () => {
      cy.visit('/?distance=-20')
      cy.get('[data-testid="distance"]').should('have.value', '0') // default
    })
  })

  describe('Time', () => {
    it('retrieves the value from URL', () => {
      cy.visit('/?time=1h23m45s')
      cy.get('[data-testid="time-hours"]').should('have.value', '1')
      cy.get('[data-testid="time-minutes"]').should('have.value', '23')
      cy.get('[data-testid="time-seconds"]').should('have.value', '45')
    })
    it('accepts only numbers', () => {
      cy.visit('/?time=abc')
      cy.get('[data-testid="time-hours"]').should('have.value', '0') // default
      cy.get('[data-testid="time-minutes"]').should('have.value', '0') // default
      cy.get('[data-testid="time-seconds"]').should('have.value', '0') // default
      cy.get('[data-testid="pace-minutes"').should('have.value', '6') // default
      cy.get('[data-testid="pace-seconds"').should('have.value', '0') // default
    })
    it('accepts single time component (hours)', () => {
      cy.visit('/?time=1h')
      cy.get('[data-testid="time-hours"]').should('have.value', '1')
      cy.get('[data-testid="time-minutes"]').should('have.value', '0')
      cy.get('[data-testid="time-seconds"]').should('have.value', '0')
      cy.get('[data-testid="pace-minutes"').should('have.value', '6') // default
      cy.get('[data-testid="pace-seconds"').should('have.value', '0') // default
    })
    it('accepts single time component (minutes)', () => {
      cy.visit('/?time=20m')
      cy.get('[data-testid="time-hours"]').should('have.value', '0')
      cy.get('[data-testid="time-minutes"]').should('have.value', '20')
      cy.get('[data-testid="time-seconds"]').should('have.value', '0')
      cy.get('[data-testid="pace-minutes"').should('have.value', '6') // default
      cy.get('[data-testid="pace-seconds"').should('have.value', '0') // default
    })
    it('accepts single time component (seconds)', () => {
      cy.visit('/?time=30s')
      cy.get('[data-testid="time-hours"]').should('have.value', '0')
      cy.get('[data-testid="time-minutes"]').should('have.value', '0')
      cy.get('[data-testid="time-seconds"]').should('have.value', '30')
      cy.get('[data-testid="pace-minutes"').should('have.value', '6') // default
      cy.get('[data-testid="pace-seconds"').should('have.value', '0') // default
    })
    it('allows exceeding input maximum value(s)', () => {
      cy.visit('/?time=82m105s')
      cy.get('[data-testid="time-hours"]').should('have.value', '1')
      cy.get('[data-testid="time-minutes"]').should('have.value', '23')
      cy.get('[data-testid="time-seconds"]').should('have.value', '45')
    })
  })
})
