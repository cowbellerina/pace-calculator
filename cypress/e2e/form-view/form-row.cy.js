describe('Form Row', () => {
  describe('Distance form', () => {
    beforeEach(() => {
      cy.visit('/')
    })

    it('selects input text on focus', () => {
      cy.get('[data-testid="distance"]')
        .type('1').blur()
        .type('2').should('have.value', '2')
    })
    it('disables input when locked', () => {
      cy.get(`[data-testid="distance-lock"]`).click()
      cy.get('[data-testid="distance"]').should('be.disabled')
    })
    it('has sensible min value', () => {
      cy.get('[data-testid="distance"]')
        .type('0').should('have.value', '0')
      cy.get('[data-testid="distance"]')
        .type('{downarrow}').should('have.value', '0')
    })
    it('has sensible max value', () => {
      cy.get('[data-testid="distance"]')
        .type('1000').should('have.value', '1000')
      cy.get('[data-testid="distance"]')
        .type('{uparrow}').should('have.value', '1000')
    })
    it('updates distance when selecting one from distances list', () => {
      cy.get('[data-testid="distance-list"]')
        .select('10k')
      cy.get('[data-testid="distance"]')
        .should('have.value', '10')
    })
    it('updates distance list when distance changes', () => {
      cy.get('[data-testid="distance"]')
        .clear().type('9.9')
      cy.get('[data-testid="distance-list"]')
        .should('have.value', 'custom')

      cy.get('[data-testid="distance"]')
        .type('{uparrow}').trigger('change') // trigger is important, otherwise the select menu won't change
      cy.get('[data-testid="distance-list"]')
        .should('have.value', '10k')
    })
    it('allows emptying all values', () => {
      cy.get('[data-testid="pace-minutes"]').type('0')
      cy.get('[data-testid="pace-seconds"]').type('0')
      cy.get('[data-testid="time-hours"]').type('0')
      cy.get('[data-testid="time-minutes"]').type('0')
      cy.get('[data-testid="time-seconds"]').type('0')

      cy.get('[data-testid="distance"')
        .type('0').should('have.value', '0')
    })
  })

  describe('Pace form', () => {
    beforeEach(() => {
      cy.visit('/')
    })

    it('selects input text on focus', () => {
      ['pace-minutes', 'pace-seconds'].forEach((id) => {
        cy.get(`[data-testid="${id}"]`)
          .type('1').blur()
          .type('2').should('have.value', '2')
      })
    })
    it('disables input when locked', () => {
      cy.get(`[data-testid="pace-lock"]`).click()
      cy.get('[data-testid="pace-minutes"]').should('be.disabled')
      cy.get('[data-testid="pace-seconds"]').should('be.disabled')
    })
    it('has sensible min value: minutes', () => {
      cy.get(`[data-testid="pace-seconds"]`)
        .type('0')
      cy.get(`[data-testid="pace-minutes"]`)
        .type('0').should('have.value', '0')
        .type('{downarrow}').should('have.value', '0')
    })
    it('has sensible min value: seconds', () => {
      cy.get(`[data-testid="pace-seconds"]`)
        .type('0').should('have.value', '0')
        .type('{downarrow}').should('have.value', '0')
    })
    it('has sensible max value', () => {
      ['pace-minutes', 'pace-seconds'].forEach((id) => {
        cy.get(`[data-testid="${id}"]`)
          .type('59').should('have.value', '59')
          .type('{uparrow}').should('have.value', '59')
      })
    })
    it('changes max value when the pace is slow', () => {
      cy.get('[data-testid="distance"]').type('{uparrow}').trigger('change')
      cy.get('[data-testid="time-hours').type('{uparrow}').trigger('change')
      cy.get('[data-testid="pace-minutes"]')
          .should('have.value', '606') // 10 hour 6 minutes / km
          .type('{uparrow}')
          .should('have.value', '607')
    })
    it('allows emptying all values', () => {
      cy.get('[data-testid="distance"]').type('0')
      cy.get('[data-testid="time-hours"]').type('0')
      cy.get('[data-testid="time-minutes"]').type('0')
      cy.get('[data-testid="time-seconds"]').type('0')

      cy.get('[data-testid="pace-minutes"')
        .type('0').should('have.value', '0')
      cy.get('[data-testid="pace-seconds"')
        .type('0').should('have.value', '0')
    })
  })

  describe('Time form', () => {
    beforeEach(() => {
      cy.visit('/')
    })

    it('selects input text on focus', () => {
      ['time-hours', 'time-minutes', 'time-seconds'].forEach((id) => {
        cy.get(`[data-testid="${id}"]`)
          .type('1').blur()
          .type('2').should('have.value', '2')
      })
    })
    it('disables input when locked', () => {
      cy.get(`[data-testid="time-lock"]`).click()
      cy.get('[data-testid="time-hours"]').should('be.disabled')
      cy.get('[data-testid="time-minutes"]').should('be.disabled')
      cy.get('[data-testid="time-seconds"]').should('be.disabled')
    })
    it('has sensible min value: hours', () => {
      cy.get(`[data-testid="time-hours"]`)
        .type('0').should('have.value', '0')
        .type('{downarrow}').should('have.value', '0')
    })
    it('has sensible min value: minutes', () => {
      cy.get(`[data-testid="time-minutes"]`)
        .type('0').should('have.value', '0')
        .type('{downarrow}').should('have.value', '0')
    })
    it('has sensible min value: seconds', () => {
      cy.get(`[data-testid="time-seconds"]`)
        .type('0').should('have.value', '0')
        .type('{downarrow}').should('have.value', '0')
    })
    it('has sensible max value', () => {
      cy.get('[data-testid="time-hours"]')
        .type('23').should('have.value', '23')
        .type('{uparrow}').should('have.value', '23');

      ['time-minutes', 'time-seconds'].forEach((id) => {
        cy.get(`[data-testid="${id}"]`)
          .type('59').should('have.value', '59')
          .type('{uparrow}').should('have.value', '59')
      })
    })
    it('allows emptying all values', () => {
      cy.get('[data-testid="distance"]').type('0')
      cy.get('[data-testid="pace-minutes"]').type('0')
      cy.get('[data-testid="pace-seconds"]').type('0')

      cy.get('[data-testid="time-hours"')
        .type('0').should('have.value', '0')
      cy.get('[data-testid="time-minutes"')
        .type('0').should('have.value', '0')
      cy.get('[data-testid="time-seconds"')
        .type('0').should('have.value', '0')
    })
  })

  describe('Calculations', () => {
    describe('when performing calculations without parameter lock', () => {
      beforeEach(() => {
        cy.visit('/')
        cy.get('[data-testid="distance"]').type('42.2')
      })

      it('updates time on distance change', () => {
        cy.get('[data-testid="pace-minutes"]').should('have.value', '6')
        cy.get('[data-testid="pace-seconds"]').should('have.value', '0')

        cy.get('[data-testid="distance"]').clear().type('13.8')

        // Pace should remain the same as before
        cy.get('[data-testid="pace-minutes"]').should('have.value', '6')
        cy.get('[data-testid="pace-seconds"]').should('have.value', '0')

        cy.get('[data-testid="time-hours"]').should('have.value', '1')
        cy.get('[data-testid="time-minutes"]').should('have.value', '22')
        cy.get('[data-testid="time-seconds"]').should('have.value', '48')
      })
      it('updates time on pace change', () => {
        cy.get('[data-testid="distance"]').should('have.value', '42.2')

        cy.get('[data-testid="pace-minutes"]').type('7')

        // Distance should remain the same as before
        cy.get('[data-testid="distance"]').should('have.value', '42.2')

        cy.get('[data-testid="time-hours"]').should('have.value', '4')
        cy.get('[data-testid="time-minutes"]').should('have.value', '55')
        cy.get('[data-testid="time-seconds"]').should('have.value', '24')
      })
      it('updates pace on time change', () => {
        cy.get('[data-testid="distance"]').should('have.value', '42.2')

        cy.get('[data-testid="time-hours"]').type('5')
        cy.get('[data-testid="time-minutes"]').type('0')
        cy.get('[data-testid="time-seconds"]').type('0')

        // Distance should remain the same as before
        cy.get('[data-testid="distance"]').should('have.value', '42.2')

        cy.get('[data-testid="pace-minutes"]').should('have.value', '7')
        cy.get('[data-testid="pace-seconds"]').should('have.value', '7')
      })
    })

    describe('when performing calculations using the parameter lock', () => {
      beforeEach(() => {
        cy.visit('/')
        cy.get('[data-testid="distance"]')
          .type('10.0')
          .should('have.value', '10.0')

        cy.get('[data-testid="pace-minutes"]').should('have.value', '6')
        cy.get('[data-testid="pace-seconds"]').should('have.value', '0')

        cy.get('[data-testid="time-hours"]').should('have.value', '1')
        cy.get('[data-testid="time-minutes"]').should('have.value', '0')
        cy.get('[data-testid="time-seconds"]').should('have.value', '0')
      })

      it('when time is locked it updates pace on distance change', () => {
        cy.get('[data-testid="time-lock"]').click()
        cy.get('[data-testid="distance"]').clear().type('12.0')

        // Time should remain the same as before
        cy.get('[data-testid="time-hours"]').should('have.value', '1')
        cy.get('[data-testid="time-minutes"]').should('have.value', '0')
        cy.get('[data-testid="time-seconds"]').should('have.value', '0')

        cy.get('[data-testid="pace-minutes"]').should('have.value', '5')
        cy.get('[data-testid="pace-seconds"]').should('have.value', '0')
      })
      it('when pace is locked it updates time on distance change', () => {
        cy.get('[data-testid="pace-lock"]').click()
        cy.get('[data-testid="distance"]').clear().type('12.0')

        // Pace should remain the same as before
        cy.get('[data-testid="pace-minutes"]').should('have.value', '6')
        cy.get('[data-testid="pace-seconds"]').should('have.value', '0')

        cy.get('[data-testid="time-hours"]').should('have.value', '1')
        cy.get('[data-testid="time-minutes"]').should('have.value', '12')
        cy.get('[data-testid="time-seconds"]').should('have.value', '0')
      })

      it('when distance is locked it updates time on pace change', () => {
        cy.get('[data-testid="distance-lock"]').click()
        cy.get('[data-testid="pace-minutes"]').type('5')

        // Distance should remain the same as before
        cy.get('[data-testid="distance"]').should('have.value', '10.0')

        cy.get('[data-testid="time-hours"]').should('have.value', '0')
        cy.get('[data-testid="time-minutes"]').should('have.value', '50')
        cy.get('[data-testid="time-seconds"]').should('have.value', '0')
      })
      it('when time is locked it updates distance on pace change', () => {
        cy.get('[data-testid="time-lock"]').click()
        cy.get('[data-testid="pace-minutes"]').type('5')

        // Time should remain the same as before
        cy.get('[data-testid="time-hours"]').should('have.value', '1')
        cy.get('[data-testid="time-minutes"]').should('have.value', '0')
        cy.get('[data-testid="time-seconds"]').should('have.value', '0')

        cy.get('[data-testid="distance"]').should('have.value', '12')
      })

      it('when distance is locked it updates pace on time change', () => {
        cy.get('[data-testid="distance-lock"]').click()
        cy.get('[data-testid="time-hours"]').type('0')
        cy.get('[data-testid="time-minutes"]').type('45')
        cy.get('[data-testid="time-seconds"]').type('30')

        // Distance should remain the same as before
        cy.get('[data-testid="distance"]').should('have.value', '10.0')

        cy.get('[data-testid="pace-minutes"]').should('have.value', '4')
        cy.get('[data-testid="pace-seconds"]').should('have.value', '33')
      })
      it('when pace is locked it updates distance on time change', () => {
        cy.get('[data-testid="pace-lock"]').click()
        cy.get('[data-testid="time-hours"]').type('0')
        cy.get('[data-testid="time-minutes"]').type('45')
        cy.get('[data-testid="time-seconds"]').type('30')

        // Pace should remain the same as before
        cy.get('[data-testid="pace-minutes"]').should('have.value', '6')
        cy.get('[data-testid="pace-seconds"]').should('have.value', '0')

        cy.get('[data-testid="distance"]').should('have.value', '7.6')
      })
    })
  })
})

describe('Multiple Form Rows', () => {
  beforeEach(() => {
    cy.visit('/')
  })

  it('adds and deletes form rows with buttons', () => {
    cy.get('[data-testid="form-row-0"]').should('be.visible')
    cy.get('[data-testid="form-row-1"]').should('not.exist')
    cy.get('[data-testid="form-row-2"]').should('not.exist')

    // Add two new rows
    cy.get('[data-testid="segments-add"]').click()
    cy.get('[data-testid="segments-add"]').click()

    cy.get('[data-testid="form-row-0"]').should('be.visible')
    cy.get('[data-testid="form-row-1"]').should('be.visible')
    cy.get('[data-testid="form-row-2"]').should('be.visible')

    // Delete the row in the middle
    cy.get('[data-testid="form-row-1"] [data-testid="segments-delete"]').click()

    cy.get('[data-testid="form-row-0"]').should('be.visible')
    cy.get('[data-testid="form-row-1"]').should('be.visible')
    cy.get('[data-testid="form-row-2"]').should('not.exist')
  })
})
