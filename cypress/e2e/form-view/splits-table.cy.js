describe('Splits Table', () => {
  describe('one segment', () => {
    beforeEach(() => {
      cy.visit('/')
    })
    it('does not render if no data', () => {
      cy.get('[data-testid="distance"]')
        .clear().type('0')
      cy.get('[data-testid="table-splits"]').should('not.exist')
      cy.get('[data-testid="table-splits-results"] > tr').should('have.length', 0)
    })
    it('renders distances shorter than one split', () => {
      cy.get('[data-testid="distance"]')
        .clear().type('0.2')
      cy.get('[data-testid="table-splits-results"] > tr').should('have.length', 1)
    })
    it('renders one row per starting split', () => {
      cy.get('[data-testid="distance"]')
        .clear().type('10.1')
      cy.get('[data-testid="table-splits-results"] > tr').should('have.length', 11)
    })
    it('highlights the predefined and selected distance', () => {
      cy.get('[data-testid="distance"]')
        .clear().type('10.1')
      cy.get('[data-testid="table-splits-results"] > tr:nth-child(5) > td').should('have.class', 'bg-grey-lighter font-bold')
      cy.get('[data-testid="table-splits-results"] > tr:nth-child(10) > td').should('have.class', 'bg-grey-lighter font-bold')
      cy.get('[data-testid="table-splits-results"] > tr:last-child > td').should('have.class', 'bg-orange-lightest font-bold')
    })
  })

  describe('multiple segments', () => {
    beforeEach(() => {
      cy.visit('/?distance=5.1&distance=4.2&time=0h30m0s&time=0h25m0s')
    })
    it('renders one heading row per segment and one row per starting split', () => {
      cy.get('[data-testid="table-splits-results"] > tr').should('have.length', 14)
    })
    it('renders pace separately for each segment', () => {
      cy.get('[data-testid="table-head-pace-0"]').should('have.text', '05:52 min / km')
      cy.get('[data-testid="table-head-pace-1"]').should('have.text', '05:57 min / km')
    })
    it('renders the total distance and time', () => {
      cy.get('[data-testid="table-splits-results"] > tr:last-child > td:nth-child(4)').should('have.text', '9.3 km')
      cy.get('[data-testid="table-splits-results"] > tr:last-child > td:nth-child(5)').should('have.text', '00:55:00')
    })

    it('handles unit conversations', () => {
      cy.get('[data-testid="unit"]').select('mi')

      cy.get('[data-testid="table-splits-results"] > tr').should('have.length', 10)

      cy.get('[data-testid="table-head-pace-0"]').should('have.text', '09:23 min / mi')
      cy.get('[data-testid="table-head-pace-1"]').should('have.text', '09:37 min / mi')

      cy.get('[data-testid="table-splits-results"] > tr:last-child > td:nth-child(4)').should('have.text', '5.8 mi')
      cy.get('[data-testid="table-splits-results"] > tr:last-child > td:nth-child(5)').should('have.text', '00:55:01') // rounding error
    })
  })
})
