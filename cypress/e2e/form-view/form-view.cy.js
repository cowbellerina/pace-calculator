describe('Form View', () => {
  beforeEach(() => {
    cy.visit('/')
  })
  it('loads with default data', () => {
    cy.get('[data-testid="distance"]').should('have.value', '0')
    cy.get('[data-testid="distance-list"]').should('have.value', 'custom')
    cy.get('[data-testid="pace-minutes"]').should('have.value', '6')
    cy.get('[data-testid="pace-seconds"]').should('have.value', '0')
    cy.get('[data-testid="time-hours"]').should('have.value', '0')
    cy.get('[data-testid="time-minutes"]').should('have.value', '0')
    cy.get('[data-testid="time-seconds"]').should('have.value', '0')
  })
  it('shows welcome view when distance is not defined', () => {
    cy.get('[data-testid="welcome-view"]').should('be.visible')
    cy.get('[data-testid="table-splits-results"]').should('not.exist')
  })
  it('hides welcome view when distance is defined', () => {
    cy.get('[data-testid="distance"]').clear().type('1')
    cy.get('[data-testid="welcome-view"]').should('not.exist')
    cy.get('[data-testid="table-splits-results"]').should('be.visible')
  })
  it('updates the form row values when selecting a predefined distance', () => {
    cy.contains('10k in 45 minutes').click()
    cy.get('[data-testid="distance"]').should('have.value', '10')
    cy.get('[data-testid="time-minutes"]').should('have.value', '45')
    cy.get('[data-testid="table-splits-results"]').should('be.visible')
  })

  describe('Settings', () => {
    describe('Unit', () => {
      beforeEach(() => {
        cy.visit('/')
        cy.get('[data-testid="distance"]').type('10')
      })

      it('has kilometers on by default', () => {
        cy.get('[data-testid="unit"]').should('have.value', 'km')
        cy.get('[data-testid="distance-unit"]').should('have.text', 'km')
        cy.get('[data-testid="table-head-distance"]').should('contain', 'km')
        cy.get('[data-testid="table-head-pace"]').should('contain', 'km')
        cy.get('[data-testid="table-splits-results"] > tr:first-child > td').should('contain', 'km')
      })
      it('updates labels on unit changes', () => {
        cy.get('[data-testid="unit"]').select('mi')
        cy.get('[data-testid="distance-unit"]').should('have.text', 'mi')
        cy.get('[data-testid="table-head-distance"]').should('contain', 'mi')
        cy.get('[data-testid="table-head-pace"]').should('contain', 'mi')
        cy.get('[data-testid="table-splits-results"] > tr:first-child > td').should('contain', 'mi')
      })
      it('converts distance and pace on unit change', () => {
        // Kilometers to miles
        cy.get('[data-testid="unit"]').select('mi')
        cy.get('[data-testid="distance"]').should('have.value', '6.2')
        cy.get('[data-testid="pace-minutes"]').should('have.value', '9')
        cy.get('[data-testid="pace-seconds"]').should('have.value', '41')

        // Miles to kilometers
        cy.get('[data-testid="unit"]').select('km')
        cy.get('[data-testid="distance"]').should('have.value', '10')
        cy.get('[data-testid="pace-minutes"]').should('have.value', '6')
        cy.get('[data-testid="pace-seconds"]').should('have.value', '0')
      })
      it('allows changing unit with empty form', () => {
        cy.get('[data-testid="distance"]').clear()
        cy.get('[data-testid="pace-minutes"]').clear()

        cy.get('[data-testid="unit"]').select('mi')

        cy.get('[data-testid="distance"]').should('have.value','0')
        cy.get('[data-testid="pace-minutes"]').should('have.value','0')
        cy.get('[data-testid="pace-seconds"]').should('have.value','0')
        cy.get('[data-testid="time-hours"]').should('have.value','0')
        cy.get('[data-testid="time-minutes"]').should('have.value','0')
        cy.get('[data-testid="time-seconds"]').should('have.value','0')
      })
    })
  })
})
