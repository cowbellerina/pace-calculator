import stubHasTouchMediaQuery from '../../support/browser'

describe('Form Row (touch)', () => {
  describe('Distance form (touch)', () => {
    beforeEach(() => {
      cy.viewport('iphone-6')
      cy.visit('/', {
        onBeforeLoad(win) {
          stubHasTouchMediaQuery(win)
        }
      })
    })

    it('updates distance integers when selecting one from distances list', () => {
      cy.get('[data-testid="distance-integers"]')
        .select('11')
        .should('have.value', '11')
      cy.get('[data-testid="distance-decimals"]')
        .select('42')
        .should('have.value', '42')
    })
  })
})
