export default function stubHasTouchMediaQuery(win) {
  return cy.stub(win, 'matchMedia')
    .withArgs('(any-pointer: coarse)')
    .returns({
      matches: true
    })
    .as('has-touch')
}
