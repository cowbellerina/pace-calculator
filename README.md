# Running Pace Calculator

> Calculate distance, pace, or time for your run with ease.

![figure](./screenshots/v03-views.png "Running Pace Calculator")

## Introduction

The motivation for building this application is to learn and maintain web application development skills,
and to experiment on technology.

Alternative running pace calculators available in the web are
[Strava](https://www.strava.com/running-pace-calculator),
[Polar](https://www.polar.com/blog/running-pace-calculator),
and probably a thousand others.

##### Tech stack

- The application is built with [React](https://reactjs.org) using
  [Create React App](https://create-react-app.dev)
- The styles are mostly out-of-the-box [Tailwind CSS](https://tailwindcss.com) utility classes
  processed with [PostCSS](https://postcss.org)
- Integration tests are written for, and runnable using, [Cypress](https://www.cypress.io)
- Unit tests are written with the help of [React Testing Library](https://testing-library.com/docs/react-testing-library/intro/),
  and runnable using [Jest](https://jestjs.io)
 

## Install

```sh
git clone git@gitlab.com:cowbellerina/pace-calculator.git
cd pace-calculator
npm install
```

## Usage

### `npm start`

Runs the application in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `npm run build:css`, `npm run watch:css`

Processes application styles with PostCSS.

### `npm run cypress:open`

Launches the integration test runner (Cypress) in the interactive GUI.<br>
The application needs to be running in order to run the integration tests.

### `npm run cypress:run`

Runs the integration tests from the CLI without the GUI.<br>
The application needs to be running in order to run the integration tests.

### `npm test`

Launches the unit test runner (Jest) in the interactive watch mode.

### `npm run lint`, `npm run lint:fix` 

Runs the source code against the lint rules.

### `npm run build`

Builds the application for production to the `build` folder.

### `npm run serve`

Serves the production build (`build` directory) using a static file server.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

---

For detailed explanation on how things work, please see the Create React App v5 [documentation](https://create-react-app.dev/docs/getting-started).

## Changelog

Please see the [changelog](./CHANGELOG.md).

## License

Licensed under the [MIT license](https://gitlab.com/cowbellerina/pace-calculator/raw/master/LICENSE).
