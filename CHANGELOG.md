Changelog
=========

### 3.0.3 (February 25, 2023)
* No functional changes
  * Improve utility code quality and test coverage 
  * Add lint step to CI pipeline run

### 3.0.2 (February 20, 2023)
* Serve asset files relative to index file
  * (enables serving the app from subdirectory or root)

### 3.0.0 (February 19, 2023)
* Add support for segments (for phased runs)
* Update dependencies to latest available versions

### 2.2.0 (August 29, 2022)
* Convert class-based React components to functional components
* Add lint tooling and configuration
* Update dependencies to latest available versions

### 2.1.0 (September 17, 2019)
* Add separate field for inputting distance decimals on touch devices

### 2.0.1 (July 23, 2019)
* Fix the abbreviation used for mile (ml -> mi) in the predefined distances list
* Add distinctive background color for input widgets based on their state

### 2.0.0 (July 21, 2019)
* Improve touch device support
  * Switch `<input>` fields to `<select>` fields in touch devices for improved
    user experience
* Add parameter lock functionality
  * Distance, pace, or time may be locked to their current value, making it
    easier to perform certain calculations.
* Fix the abbreviation used for mile (ml -> mi)
* Fix input handling so division by zero does not occur

### 1.0.0 (January 22, 2019)
* Initial release
