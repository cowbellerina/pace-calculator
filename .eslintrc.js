module.exports = {
    env: {
        browser: true,
        es2021: true,
    },
    globals: {
        cy: 'readonly',
    },
    extends: [
        'react-app',
        'react-app/jest',
    ],
    overrides: [],
    parserOptions: {
        ecmaVersion: 'latest',
        sourceType: 'module',
    },
    plugins: [
        'react',
        'cypress',
    ],
    rules: {},
}
