import React, {useEffect, useState} from 'react'

import {fetchState, saveState} from '../utilities/storage'

function getUrlParams() {
  const queryParams = new URLSearchParams(window.location.search)
  const params = {}

  const unit = queryParams.get('unit')
  if (unit && ['km', 'mi'].indexOf(unit) !== -1) {
    params.unit = unit
  }

  return params
}

export const defaultSettings = {
  unit: 'km',
  split: 1,
  distances: [
    {id: 'marathon', name: 'Marathon', length: 42.2, unit: 'km'},
    {id: 'half-marathon', name: 'Half-Marathon', length: 21.1, unit: 'km'},
    {id: '5k', name: '5k', length: 5, unit: 'km'},
    {id: '10k', name: '10k', length: 10, unit: 'km'},
    {id: '10mi', name: '10 mi', length: 10, unit: 'mi'},
    {id: '50k', name: '50k', length: 50, unit: 'km'},
  ]
}

let SettingsContext
const {Provider, Consumer} = SettingsContext = React.createContext(defaultSettings)

function SettingsProvider({children}) {
  function onChangeSettings({unit}) {
    setSettings({...settings, unit})
  }

  const [settings, setSettings] = useState(() => {
    const localStorageState = fetchState()
    let localStorageSettings = {}
    if (localStorageState && localStorageState.settings) {
      localStorageSettings = localStorageState.settings
    }

    const urlSettings = getUrlParams()

    return Object.assign({}, defaultSettings, localStorageSettings, urlSettings)
  })

  useEffect(() => {
    const localStorageState = fetchState()
    if (!localStorageState || localStorageState.unit !== settings.unit) {
      saveState({settings: {unit: settings.unit}})
    }
  }, [settings.unit])

  return (
    <Provider value={{...settings, onChangeSettings}}>
      {children}
    </Provider>
  )
}

export {SettingsProvider, Consumer as SettingsConsumer, SettingsContext}
