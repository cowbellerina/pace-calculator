import React from 'react'
import PropTypes from 'prop-types'
import {FaLock, FaUnlock} from 'react-icons/fa'

import {SettingsConsumer} from '../../contexts/SettingsContext'

import C from '../../utilities/constants'
import {extractDecimals, extractIntegers} from '../../utilities/common'
import {hasTouch} from '../../utilities/browser'
import {addTimeUnitToTime, secondsToTimeUnits} from '../../utilities/time'

import NumberInput from './NumberInput'
import DistanceSelect from './DistanceSelect'

function LockButton({isLocked = false, ...props}) {
  return (<button className="cursor-pointer" {...props}>{isLocked ? <FaLock/> : <FaUnlock/>}</button>)
}

function FormRow(props) {
  function onChangeDistance(property) {
    return (event) => {
      let {value: distance} = event.currentTarget
      distance = distance === '' ? 0 : distance
      if (isFinite(distance)) {
        distance = parseFloat(distance)
      }

      if (property === 'integer') {
        const decimals = extractDecimals(props.distance)
        props.onChangeDistance(distance + (decimals / 100))
      } else if (property === 'decimal') {
        const integers = extractIntegers(props.distance)
        props.onChangeDistance(integers + (distance / 100))
      } else {
        props.onChangeDistance(distance)
      }
    }
  }

  function onChangeTimeUnit(property, unit) {
    return (event) => {
      let {value: timeUnit} = event.currentTarget
      timeUnit = timeUnit === '' ? 0 : timeUnit
      if (isFinite(timeUnit)) {
        timeUnit = ~~timeUnit
      }

      if (property === 'pace') {
        const pace = addTimeUnitToTime(props.pace, timeUnit, unit)
        props.onChangePace(pace)
      } else if (property === 'time') {
        const time = addTimeUnitToTime(props.time, timeUnit, unit)
        props.onChangeTime(time)
      }
    }
  }

  function onClickLock(lock) {
    return (event) => {
      event.preventDefault()
      props.onClickLock(lock)
    }
  }
  
  const {hours: paceHours, minutes: paceMinutes, seconds: paceSeconds} = secondsToTimeUnits(props.pace)
  const {hours: timeHours, minutes: timeMinutes, seconds: timeSeconds} = secondsToTimeUnits(props.time)

  const isSlowPace = paceHours > 0
  const widget = hasTouch() ? 'select' : 'input'

  const isDistanceLocked = props.lock === C.PARAM_NAME_DISTANCE
  const isPaceLocked = props.lock === C.PARAM_NAME_PACE
  const isTimeLocked = props.lock === C.PARAM_NAME_TIME

  return (
    <SettingsConsumer>
      {({unit}) => (
        <div className="c-form-row">

          <div className="c-form-row-item c-form-row-item-distance">
            <label htmlFor="distanceLabel" className="block mb-1 font-light">
              Distance <LockButton isLocked={isDistanceLocked}
                                   onClick={onClickLock('distance')}
                                   data-testid="distance-lock"/>
            </label>
            {hasTouch()
              ? <React.Fragment>
                  <div className="flex flex-row -mx-1">
                    <div className="flex-1 mx-1">
                      <NumberInput
                        id={'distance-integers'}
                        disabled={isDistanceLocked}
                        onChange={onChangeDistance('integer')}
                        value={extractIntegers(props.distance)}
                        unit={unit}
                        selectRange={[0, 100]}
                        widget={'select'}
                      />
                    </div>
                    <div className="flex-1 mx-1">
                      <NumberInput
                        id={'distance-decimals'}
                        disabled={isDistanceLocked}
                        onChange={onChangeDistance('decimal')}
                        value={extractDecimals(props.distance)}
                        unit={unit === C.PARAM_VALUE_MI ? '' : 'dam'}
                        selectRange={[0, 100]}
                        widget={'select'}/>
                    </div>
                  </div>
                  <div className="mt-2">
                    <DistanceSelect
                      distance={props.distance}
                      onChange={props.onChangeDistance}
                    />
                  </div>
                </React.Fragment>
              : <div className="flex items-center -mx-1 md:flex-col md:items-stretch xxl:flex-row xxl:items-center">
                  <div className="flex-1 xxl:flex-auto mx-1">
                    <NumberInput
                      id={'distance'}
                      disabled={isDistanceLocked}
                      onChange={onChangeDistance(null)}
                      value={props.distance}
                      unit={unit}
                      inputProps={{min: 0, max: 1000, step: '0.1'}}
                      widget={'input'}
                    />
                  </div>
                  <div className="flex-1 xxl:flex-auto mx-1" style={{minWidth: 100}}>
                    <DistanceSelect
                      distance={props.distance}
                      onChange={props.onChangeDistance}
                    />
                  </div>
                </div>
            }
          </div>

          <div className="c-form-row-item c-form-row-item-pace">
            <label htmlFor="pace" className="block mb-1 font-light">
              Pace <LockButton isLocked={isPaceLocked}
                               onClick={onClickLock('pace')}
                               data-testid="pace-lock"/>
            </label>
            <div className="flex md:flex-none flex-row -mx-1">
              <div className="flex-1 mx-1">
                <NumberInput
                  id={'pace-minutes'}
                  disabled={isPaceLocked}
                  onChange={onChangeTimeUnit('pace', 'm')}
                  value={isSlowPace ? (paceHours * 60) + paceMinutes : paceMinutes}
                  unit={'min'}
                  inputProps={{min: 0, max: (isSlowPace ? 1000 : 59)}}
                  selectRange={[0, (isSlowPace ? 100 : 59)]}
                  widget={widget}
                />
              </div>
              <div className="flex-1 mx-1">
                <NumberInput
                  id={'pace-seconds'}
                  disabled={isPaceLocked}
                  onChange={onChangeTimeUnit('pace', 's')}
                  value={paceSeconds}
                  unit={'sec'}
                  inputProps={{min: 0, max: 59}}
                  selectRange={[0, 59]}
                  widget={widget}
                />
              </div>
            </div>
          </div>

          <div className="c-form-row-item c-form-row-item-time">
            <label htmlFor="time" className="block mb-1 font-light">
              Time <LockButton isLocked={isTimeLocked}
                               onClick={onClickLock('time')}
                               data-testid="time-lock"/>
            </label>
            <div className="flex flex-row -mx-1">
              <div className="flex-1 mx-1">
                <NumberInput
                  id={'time-hours'}
                  disabled={isTimeLocked}
                  onChange={onChangeTimeUnit('time', 'h')}
                  value={timeHours}
                  unit={'hours'}
                  inputProps={{min: 0, max: 23}}
                  selectRange={[0, 23]}
                  widget={widget}
                />
              </div>
              <div className="flex-1 mx-1">
                <NumberInput
                  id={'time-minutes'}
                  disabled={isTimeLocked}
                  onChange={onChangeTimeUnit('time', 'm')}
                  value={timeMinutes}
                  unit={'min'}
                  inputProps={{min: 0, max: 59}}
                  selectRange={[0, 59]}
                  widget={widget}
                />
              </div>
              <div className="flex-1 mx-1">
                <NumberInput
                  id={'time-seconds'}
                  disabled={isTimeLocked}
                  onChange={onChangeTimeUnit('time', 's')}
                  value={timeSeconds}
                  unit={'sec'}
                  inputProps={{min: 0, max: 59}}
                  selectRange={[0, 59]}
                  widget={widget}
                />
              </div>
            </div>
          </div>

        </div>
      )}
    </SettingsConsumer>
  )
}

FormRow.propTypes = {
  distance: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
  ]).isRequired,
  pace: PropTypes.number.isRequired,
  time: PropTypes.number.isRequired,
  lock: PropTypes.string,
  onChangeDistance: PropTypes.func.isRequired,
  onChangePace: PropTypes.func.isRequired,
  onChangeTime: PropTypes.func.isRequired,
  onClickLock: PropTypes.func.isRequired
}

export default FormRow
