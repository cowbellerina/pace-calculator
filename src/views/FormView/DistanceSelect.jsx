import React, {useContext} from 'react'
import PropTypes from 'prop-types'

import {SettingsConsumer, SettingsContext} from '../../contexts/SettingsContext'

import C from '../../utilities/constants'
import {formatDistance} from '../../utilities/common'

function DistanceSelect(props) {
  function getPredefinedDistanceById(id) {
    return distances.find((distance) => distance.id === id)
  }

  function getPredefinedDistanceByLength(length) {
    const distancesInCurrentUnit = distances.filter((distance) => distance.unit === unit)
    let distance = distancesInCurrentUnit.find((distance) => formatDistance(distance.length) === formatDistance(length))
    if (!distance) {
      const multiplier = unit === C.PARAM_VALUE_MI ? C.RATIO_KM_TO_MI : C.RATIO_MI_TO_KM
      const lengthInOtherUnit = formatDistance(length * multiplier)
      const distancesInOtherUnit = distances.filter((distance) => distance.unit !== unit)
      distance = distancesInOtherUnit.find((distance) => formatDistance(distance.length) === lengthInOtherUnit)
    }
    return distance
  }

  function onChangeDistance(event) {
    const id = event.currentTarget.value
    const distance = getPredefinedDistanceById(id)
    if (distance && distance.length > 0) {
      let newDistance = formatDistance(distance.length)
      if (distance.unit !== unit) {
        const multiplier = unit === C.PARAM_VALUE_MI ? C.RATIO_KM_TO_MI : C.RATIO_MI_TO_KM
        newDistance = formatDistance(distance.length / multiplier)
      }
      props.onChange(newDistance)
    }
  }

  const {distances, unit} = useContext(SettingsContext)

  const predefinedDistance = getPredefinedDistanceByLength(props.distance)
  const predefinedDistanceId = predefinedDistance ? predefinedDistance.id : 'custom'

  return (
    <SettingsConsumer>
      {({distances}) => (
        <select id="distanceLabel"
                value={predefinedDistanceId}
                onChange={onChangeDistance}
                className="w-full text-xs text-center text-teal-dark underline underline-dotted cursor-pointer appearance-none bg-transparent border-0"
                data-testid="distance-list"
        >
          {distances.map((distance, index) => {
            return (
              <option key={`distance-${index}`} value={distance.id}>{distance.name}</option>
            )
          })}
          {predefinedDistanceId === 'custom' && <option value="custom">Custom</option>}
        </select>
      )}
    </SettingsConsumer>
  )
}

DistanceSelect.propTypes = {
  distance: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
  ]).isRequired,
  onChange: PropTypes.func.isRequired
}

export default DistanceSelect
