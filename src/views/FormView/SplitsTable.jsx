import React, {useContext} from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'
import {uniq} from 'lodash'

import {SettingsConsumer, SettingsContext} from '../../contexts/SettingsContext'

import {secondsToTimeUnits, toHhmmss} from '../../utilities/time'
import {getSplitsWithinDistance} from '../../utilities/splits'

function getPredefinedDistancesWithinDistance(predefinedDistances = [], distance) {
  return predefinedDistances
    .map((d) => d.length)
    .filter((d) => d && d < distance)
}

function SplitsTable({distance, pace}) {
  function getSplits() {
    let splits = [].concat(
      getSplitsWithinDistance(distance, split),
      getPredefinedDistancesWithinDistance(predefinedDistances, distance)
    )
    splits = uniq(splits) // Remove duplicates
    splits = splits.sort((a, b) => a - b) // Asc
    return splits
  }

  const {distances: predefinedDistances, split} = useContext(SettingsContext)

  const {hours: paceHours} = secondsToTimeUnits(pace)
  const isSlowPace = paceHours > 0
  const paceTime = toHhmmss(pace, {hours: isSlowPace})

  const splits = getSplits(distance)

  if (splits.length === 0) {
    return null
  }

  return (
    <SettingsConsumer>
      {({unit, distances}) => (
        <table className="w-full" data-testid="table-splits">
          <thead>
          <tr>
            <th className="px-2 py-1 font-light text-left" data-testid="table-head-distance">
              <span className="block text-sm xs:inline">Distance </span>
              <span className="block text-sm xs:inline"> ({unit})</span>
            </th>
            <th className="px-2 py-1 font-light text-right" data-testid="table-head-pace">
              <span className="block text-sm xs:inline">{paceTime} </span>
              <span className="block text-sm xs:inline">min / {unit}</span>
            </th>
          </tr>
          </thead>
          <tbody className="border border-grey-light" data-testid="table-splits-results">
          {splits.map((split, index) => {
            const time = split * pace
            const splitTime = toHhmmss(time)

            const isHighlighted = distances.map((distance) => distance.length).indexOf(split) !== -1
            const isLast = splits.length === index + 1

            const extraCssClasses = classNames({
              'bg-grey-lighter font-bold': isHighlighted,
              'bg-orange-lightest font-bold': isLast
            })

            return (
              <tr key={`row-${split}`}>
                <td className={`border-b border-grey-light px-2 py-1 text-sm text-left ${extraCssClasses}`}>{split} {unit}</td>
                <td className={`border-b border-grey-light px-2 py-1 text-sm text-right ${extraCssClasses}`}>{splitTime}</td>
              </tr>
            )
          })}
          </tbody>
        </table>
      )}
    </SettingsConsumer>
  )
}

SplitsTable.propTypes = {
  distance: PropTypes.number,
  pace: PropTypes.number
}

SplitsTable.defaultProps = {
  distance: 0,
  pace: 0
}

export default SplitsTable
