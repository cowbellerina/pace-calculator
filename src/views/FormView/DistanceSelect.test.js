import React from 'react'
import {render, fireEvent, screen} from '@testing-library/react'
import DistanceSelect from './DistanceSelect'
import {SettingsContext, defaultSettings} from '../../contexts/SettingsContext'

it('renders the component', () => {
  const onChange = jest.fn()
  render(<DistanceSelect distance={5} onChange={onChange}/>)
  const list = screen.queryByTestId('distance-list')
  expect(list).toBeInTheDocument()
})

it('renders predefined distances from context', () => {
  const onChange = jest.fn()
  const settings = Object.assign({}, defaultSettings, {
    distances: [
      {id: 'foo', name: 'Foo', length: 1.2, unit: 'km'},
      {id: 'bar', name: 'Bar', length: 3.4, unit: 'mi'},
    ]
  })
  render(
    <SettingsContext.Provider value={settings}>
      <DistanceSelect distance={1.2} onChange={onChange}/>
    </SettingsContext.Provider>
  )
  const list = screen.queryByTestId('distance-list')
  const options = Array.from(list.options)
  expect(options).toHaveLength(2)
})

it('does not render "custom" option by default', () => {
  const onChange = jest.fn()
  render(<DistanceSelect distance={5} onChange={onChange}/>)
  const list = screen.queryByTestId('distance-list')
  const customOptions = Array.from(list.options)
    .filter((option) => /custom/i.test(option.text))
  expect(customOptions).toHaveLength(0)
})

it('renders "custom" if distance is not known', async () => {
  const onChange = jest.fn()
  render(<DistanceSelect distance={31.3} onChange={onChange}/>)
  const list = screen.queryByTestId('distance-list')
  expect(list.value).toBe('custom')
  const option = screen.getByText('Custom')
  expect(option).not.toBeNull()
})

it('calls upper onChange on change', () => {
  const onChange = jest.fn()
  render(<DistanceSelect distance={5} onChange={onChange}/>)
  let list = screen.queryByTestId('distance-list')
  expect(list.value).toBe('5k')
  fireEvent.change(list, {target: {value: '10k'}})
  expect(onChange).toHaveBeenCalledTimes(1)
  expect(onChange).toHaveBeenCalledWith(10.0)
})

it('converts distances in other unit on change', () => {
  const onChange = jest.fn()
  const settings = Object.assign({}, defaultSettings, {unit: 'mi'})
  render(
    <SettingsContext.Provider value={settings}>
      <DistanceSelect distance={10} onChange={onChange}/>
    </SettingsContext.Provider>
  )
  const list = screen.queryByTestId('distance-list')
  expect(list.value).toBe('10mi')
  fireEvent.change(list, {target: {value: 'marathon'}})
  expect(onChange).toHaveBeenCalledTimes(1)
  expect(onChange).toHaveBeenCalledWith(26.2)
})
