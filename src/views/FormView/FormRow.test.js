import React from 'react'
import {render, screen} from '@testing-library/react'
import '../../mocks/matchMedia.mock'
import FormRow from './FormRow'

it('renders correctly with given values', () => {
  const onChange = jest.fn()
  const onClick = jest.fn()
  render(<FormRow
    distance={10}
    pace={365}
    time={3650}
    onChangeDistance={onChange}
    onChangePace={onChange}
    onChangeTime={onChange}
    onClickLock={onClick}
  />)

  const distance = screen.queryByTestId('distance')
  expect(distance).toBeInTheDocument()
  expect(distance.value).toBe('10')
  const distanceList = screen.queryByTestId('distance-list')
  expect(distanceList).toBeInTheDocument()
  expect(distanceList.value).toBe('10k')

  const paceMinutes = screen.queryByTestId('pace-minutes')
  expect(paceMinutes).toBeInTheDocument()
  expect(paceMinutes.value).toBe('6')
  const paceSeconds = screen.queryByTestId('pace-seconds')
  expect(paceSeconds).toBeInTheDocument()
  expect(paceSeconds.value).toBe('5')

  const timeHours = screen.queryByTestId('time-hours')
  expect(timeHours).toBeInTheDocument()
  expect(timeHours.value).toBe('1')
  const timeMinutes = screen.queryByTestId('time-minutes')
  expect(timeMinutes).toBeInTheDocument()
  expect(timeMinutes.value).toBe('0')
  const timeSeconds = screen.queryByTestId('time-seconds')
  expect(timeSeconds).toBeInTheDocument()
  expect(timeSeconds.value).toBe('50')
})
