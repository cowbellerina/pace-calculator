import React from 'react'
import PropTypes from 'prop-types'

const welcomeDistances = {
  marathon: {distance: 42.2, time: 14400, unit: 'km'},
  halfMarathon: {distance: 21.1, time: 7200, unit: 'km'},
  '10k': {distance: 10, time: 2700, unit: 'km'}
}

function WelcomeView(props) {
  function onChange(distance) {
    if (welcomeDistances[distance]) {
      props.onChange(welcomeDistances[distance])
    }
  }

  return (
    <section className="p-8 text-center text-grey-darkest" data-testid="welcome-view">
      <p className="text-xl leading-normal">Add distance, and edit pace or time to see the results</p>
      <p className="mt-3 mb-1 leading-normal">...or select one of the popular choices</p>
      <ul className="list-reset leading-normal">
        <li><button className="button-link text-teal-dark" onClick={onChange.bind(this, 'marathon')}>Marathon under 4 hours</button></li>
        <li><button className="button-link text-teal-dark" onClick={onChange.bind(this, 'halfMarathon')}>Half-Marathon under 2 hours</button></li>
        <li><button className="button-link text-teal-dark" onClick={onChange.bind(this, '10k')}>10k in 45 minutes</button></li>
      </ul>
    </section>
  )
}

WelcomeView.propTypes = {
  onChange: PropTypes.func.isRequired
}

export default WelcomeView
