import React from 'react'
import PropTypes from 'prop-types'
import ElementQuery from 'react-element-query'
import {range} from 'lodash'

function selectText(event) {
  return event.currentTarget.select()
}

const PORTRAIT_ITEM_MAX_WIDTH = 120

function NumberInput(props) {
  const {inputProps} = props
  inputProps.type = inputProps.type || 'number'

  let [rangeStart, rangeEnd] = props.selectRange
  rangeStart = rangeStart < 0 ? 0 : rangeStart
  rangeEnd = rangeEnd < rangeStart ? rangeStart : rangeEnd
  const valueOutOfBounds = props.value && (props.value > rangeStart && props.value > rangeEnd)

  return (
    <ElementQuery sizes={[
                    {name: 'c-number-input-landscape', width: PORTRAIT_ITEM_MAX_WIDTH + 1},
                    {name: 'c-number-input-portrait', width: 1}
                  ]}
                  default="c-number-input-portrait">
      <div className="c-number-input items-center border-2 border-grey-light rounded bg-grey-light">
        <div className="c-number-input-input">
          {props.widget === 'select'
            ? <select
                data-testid={props.id ? props.id : ''}
                className={`w-full pl-4 pr-2 py-2 text-xl text-grey-darkest rounded appearance-none
                            ${props.disabled ? 'bg-grey-light' : 'bg-white'}`}
                value={props.value && parseInt(props.value, 10)}
                onChange={props.onChange}
                disabled={props.disabled}
              >
                {range(rangeStart, rangeEnd + 1).map((timeUnit) => {
                  return (<option key={`${props.id}-${timeUnit}`} value={timeUnit}>{timeUnit}</option> )
                })}
                {valueOutOfBounds
                  ? <option key={`${props.id}-${props.value}`}>{props.value}</option>
                  : null
                }
            </select>
            : <input
              onFocus={selectText}
              onChange={props.onChange}
              disabled={props.disabled}
              value={props.value}
              data-testid={props.id ? props.id : ''}
              className={`w-full pl-4 pr-2 py-2 text-xl text-grey-darkest rounded appearance-none
                          ${props.disabled ? 'bg-grey-light' : 'bg-white'}`}
              {...inputProps}
            />
          }
        </div>
        {props.unit
          ? <div className="c-number-input-unit p-1 px-2 font-light text-grey-darkest text-xs"
                 data-testid={props.id ? `${props.id}-unit` : ''}
            >
              {props.unit}
            </div>
          : null
        }
      </div>
    </ElementQuery>
  )
}

NumberInput.propTypes = {
  onChange: PropTypes.func.isRequired,
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
  ]),
  disabled: PropTypes.bool,
  id: PropTypes.string,
  unit: PropTypes.string,
  inputProps: PropTypes.object,
  selectRange: PropTypes.arrayOf(PropTypes.number),
  widget: PropTypes.oneOf(['input', 'select'])
}

NumberInput.defaultProps = {
  onChange: () => {},
  disabled: false,
  inputProps: {},
  selectRange: [0, 0],
  widget: 'input'
}

export default NumberInput
