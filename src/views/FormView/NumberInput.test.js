import React from 'react'
import {render, fireEvent, screen} from '@testing-library/react'
import NumberInput from './NumberInput'

describe('when <input> is used as widget', () => {
  it('renders <input> element when "input" is passed as widget', () => {
    render(<NumberInput widget="input"/>)
    const input = screen.getByRole('spinbutton')
    expect(input).toBeDefined()
  })

  it('adds properties passed to the <input/> element', () => {
    render(<NumberInput inputProps={{type: 'text', disabled: true}}/>)
    const input = screen.getByRole('textbox')
    expect(input.type).toEqual('text')
    expect(input.disabled).toBe(true)
  })

  it('adds data-testid based on id', () => {
    render(<NumberInput id={'my-test-id'}/>)
    const input = screen.getByRole('spinbutton')
    const attribute = input.getAttribute('data-testid')
    expect(attribute).toEqual('my-test-id')
  })

  it('calls the onChange handler on change', () => {
    const onChangeHandler = jest.fn()
    render(<NumberInput onChange={onChangeHandler} />)
    const input = screen.getByRole('spinbutton')
    fireEvent.change(input, {target: {value: '11'}})
    expect(onChangeHandler).toHaveBeenCalledTimes(1)
    expect(input.value).toBe('11')
  })
})

describe('when <select> is used as widget', () => {
  it('renders <select> element when "select" is passed as widget', () => {
    render(<NumberInput widget="select"/>)
    const select = screen.getByRole('combobox')
    expect(select).toBeDefined()
  })

  it('renders <select> options for given range', () => {
    render(<NumberInput widget="select" selectRange={[0, 2]}/>)
    const select = screen.getByRole('combobox')
    expect(select.length).toEqual(3)
  })

  it('adds <select> option for values out of bounds', () => {
    const value = 5
    render(<NumberInput widget="select" selectRange={[0, 2]} value={value}/>)
    const select = screen.getByRole('combobox')
    expect(select.length).toEqual(4)
    expect(select[select.length - 1].value).toEqual(`${value}`)
  })

  it('adds data-testid based on id', () => {
    render(<NumberInput id={'my-test-id'} widget="select"/>)
    const select = screen.getByRole('combobox')
    const attribute = select.getAttribute('data-testid')
    expect(attribute).toEqual('my-test-id')
  })

  it('calls the onChange handler on change', () => {
    const onChangeHandler = jest.fn()
    render(<NumberInput onChange={onChangeHandler} widget="select" selectRange={[0, 2]}/>)
    const select = screen.getByRole('combobox')
    fireEvent.change(select, {target: {value: '1'}})
    expect(onChangeHandler).toHaveBeenCalled()
    expect(select.value).toBe('1')
  })
})

it('renders <input> element by default', () => {
  render(<NumberInput />)
  const input = screen.getByRole('spinbutton')
  expect(input.type).toEqual('number')
})

it('does not render unit if not provided', () => {
  render(<NumberInput/>)
  const unit = screen.queryByText('unit')
  expect(unit).toBeNull()
})

it('renders the unit if provided', () => {
  render(<NumberInput id={'my-test-id'} unit={'unit'}/>)
  screen.getByText('unit')
})

it('adds data-testid to the unit based on input id', () => {
  render(<NumberInput id={'my-test-id'} unit={'unit'}/>)
  const unit = screen.queryByText('unit')
  const attribute = unit.getAttribute('data-testid')
  expect(attribute).toEqual('my-test-id-unit')
})
