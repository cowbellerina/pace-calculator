import React from 'react'
import {render, screen, within} from '@testing-library/react'
import SplitsTable from './SplitsTable'
import {SettingsContext} from '../../contexts/SettingsContext'

it('does not render if no data', () => {
  render(<SplitsTable/>)
  const table = screen.queryByTestId('table-splits')
  expect(table).not.toBeInTheDocument()
})

it('renders the list', () => {
  render(<SplitsTable distance={10.1} pace={360}/>)
  const list = screen.queryByTestId('table-splits-results')
  const rows = within(list).queryAllByRole('row')
  expect(list).toBeInTheDocument()
  expect(rows.length).toEqual(11)
})

it('renders the list based on the split setting', () => {
  const settings = {unit: 'km', split: 2, distances: []}
  render(
    <SettingsContext.Provider value={settings}>
      <SplitsTable distance={10.1} pace={360}/>
    </SettingsContext.Provider>
  )
  const list = screen.queryByTestId('table-splits-results')
  const rows = within(list).queryAllByRole('row')
  expect(list).toBeInTheDocument()
  expect(rows.length).toEqual(6)
})

it('renders distances shorter than one split', () => {
  render(<SplitsTable distance={0.2} pace={360}/>)
  const list = screen.queryByTestId('table-splits-results')
  const rows = within(list).queryAllByRole('row')
  expect(list).toBeInTheDocument()
  expect(rows.length).toEqual(1)
})

it('highlights the predefined and selected distance', () => {
  render(<SplitsTable distance={5.1} pace={360}/>)
  const list = screen.queryByTestId('table-splits-results')

  const predefinedDistance = within(list).getByText('5 km')
  const targetDistance = within(list).getByText('5.1 km')

  expect(predefinedDistance).toHaveClass('bg-grey-lighter font-bold')
  expect(targetDistance).toHaveClass('bg-orange-lightest font-bold')
})
