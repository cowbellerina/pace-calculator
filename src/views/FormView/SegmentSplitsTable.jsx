import React, {useContext} from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'

import {SettingsConsumer, SettingsContext} from '../../contexts/SettingsContext'

import {secondsToTimeUnits, toHhmmss} from '../../utilities/time'
import {getSplitsBySegments} from '../../utilities/splits'

function HeadingRow({index, unit, paceTime}) {
  const cellClasses = 'bg-grey-lightest border-b border-b-grey-light px-2 py-1 font-light'
  const textClasses = 'block text-sm xs:inline'

  return (
    <tr>
      <th className={`${cellClasses} w-1/6 text-left`} data-testid={`table-head-distance-${index}`}>
        <span className={`${textClasses}`}>Split </span>
        <span className={`${textClasses}`}> ({unit})</span>
      </th>
      <th className={`${cellClasses} w-1/6 text-right`} data-testid={`table-head-pace-${index}`}>
        <span className={`${textClasses}`}>{paceTime} </span>
        <span className={`${textClasses}`}>min / {unit}</span>
      </th>
      <th className={`${cellClasses} font-bold text-center`}>
        <span className={`${textClasses}`}>#{index + 1}</span>
      </th>
      <th className={`${cellClasses} w-1/6 text-right`} data-testid={`table-head-distance-total-${index}`}>
        <span className={`${textClasses}`}>Total</span>
        <span className={`${textClasses}`}> ({unit})</span>
      </th>
      <th className={`${cellClasses} w-1/6 text-right`} data-testid={`table-head-time-total-${index}`}>
        <span className={`${textClasses}`}>Time</span>
      </th>
    </tr>
  )
}

function HeadingRowSpacer() {
  return (
    <tr>
      <th colSpan={5} className="bg-grey-light h-2.5"></th>
    </tr>
  )
}

function SegmentSplitsTable({segments}) {
  const {split: splitLength} = useContext(SettingsContext)
  const splitsBySegments = getSplitsBySegments(segments, splitLength)

  return (
    <SettingsConsumer>
      {({unit, distances}) => (
        <table className="w-full mt-4" data-testid="table-splits">
          <tbody className="border border-grey-light" data-testid="table-splits-results">
          {splitsBySegments.map(({segment, splits}, segmentIndex) => {
            const id = `segment-${segment.id}-split-${segmentIndex}`
            const isLastSegment = segmentIndex === splitsBySegments.length - 1

            const {hours: paceHours} = secondsToTimeUnits(segment.pace)
            const isSlowPace = paceHours > 0
            const paceTime = toHhmmss(segment.pace, {hours: isSlowPace})

            if (splits.length === 0) {
              return null
            }

            return (
              <React.Fragment key={id}>
                <HeadingRow index={segmentIndex} unit={unit} paceTime={paceTime}/>
                {splits.map(({splitDistance, splitTime, totalDistance, totalTime}, splitIndex) => {
                  const isHighlighted = distances.map((distance) => distance.length).indexOf(totalDistance) !== -1
                  const isLastSplit = splitIndex === splits.length - 1

                  const cellClasses = classNames({
                    'border-b border-grey-light px-2 py-1 text-sm': true,
                    'bg-grey-lighter font-bold': isHighlighted,
                    'bg-orange-lightest font-bold': isLastSegment && isLastSplit
                  })

                  return (
                    <tr key={`${id}-row-${splitIndex}`}>
                      <td className={`${cellClasses} text-left`}>{splitDistance} {unit}</td>
                      <td className={`${cellClasses} text-right`}>{toHhmmss(splitTime)}</td>
                      <td className={`${cellClasses}`}></td>
                      <td className={`${cellClasses} text-right`}>{totalDistance} {unit}</td>
                      <td className={`${cellClasses} text-right`}>{toHhmmss(totalTime)}</td>
                    </tr>
                  )
                })}
                {isLastSegment === false && <HeadingRowSpacer/>}
              </React.Fragment>
            )
          })}
          </tbody>
        </table>
      )}
    </SettingsConsumer>
  )
}

SegmentSplitsTable.propTypes = {
  segments: PropTypes.arrayOf(PropTypes.shape({
    segment: PropTypes.shape({
      id: PropTypes.string.isRequired,
      distance: PropTypes.oneOf([PropTypes.string, PropTypes.number]).isRequired,
      pace: PropTypes.number.isRequired,
      time: PropTypes.number.isRequired,
      lock: PropTypes.string,
    }),
    splits: PropTypes.arrayOf(PropTypes.shape({
      splitDistance: PropTypes.number.isRequired,
      splitTime: PropTypes.number.isRequired,
      totalDistance: PropTypes.number.isRequired,
      totalTime: PropTypes.number.isRequired,
    })),
  })).isRequired,
}

SegmentSplitsTable.defaultProps = {
  segments: [],
}

export default SegmentSplitsTable
