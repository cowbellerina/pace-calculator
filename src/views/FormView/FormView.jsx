import React, {useContext, useEffect, useReducer} from 'react'
import uniqid from 'uniqid'
import {FaMinusCircle, FaPlusCircle} from 'react-icons/fa'

import {SettingsContext} from '../../contexts/SettingsContext'
import segmentsReducer from '../../reducers/SegmentsReducer'

import C from '../../utilities/constants'
import {formatDistance} from '../../utilities/common'
import {secondsToTimeUnits} from '../../utilities/time'

import FormRow from './FormRow'
import WelcomeView from './WelcomeView'
import SegmentSplitsTable from './SegmentSplitsTable'
import SplitsTable from './SplitsTable'
import Toolbar from '../../components/Toolbar'

function getUrlParams() {
  const queryParams = new URLSearchParams(window.location.search)
  const params = {distance: [], time: []}

  const distances = queryParams.getAll(C.PARAM_NAME_DISTANCE)
  if (distances.length) {
    distances.forEach((segmentDistance) => {
      const distance = parseFloat(segmentDistance)
      if (distance && !isNaN(distance) && distance > 0) {
        params.distance.push(formatDistance(distance))
      }
    })
  }

  const times = queryParams.getAll(C.PARAM_NAME_TIME)
  if (times.length) {
    const timePattern = /(\d+h)?(\d+m)?(\d+s)?/i
    times.forEach((segmentTime) => {
      const time = segmentTime
      if (time && timePattern.test(time)) {
        let [, hours, minutes, seconds] = time.match(timePattern)
        hours = parseInt(hours, 10)
        minutes = parseInt(minutes, 10)
        seconds = parseInt(seconds, 10)
        const hoursAsSeconds = !isNaN(hours) ? hours * 60 * 60 : 0
        const minutesAsSeconds = !isNaN(minutes) ? minutes * 60 : 0
        const secondsAsSeconds = !isNaN(seconds) ? seconds : 0
        const totalTime = hoursAsSeconds + minutesAsSeconds + secondsAsSeconds
        if (totalTime > 0) {
          params.time.push(totalTime)
        }
      }
    })
  }

  return params
}

function setQueryParams(params) {
  const queryParams = new URLSearchParams(window.location.search)
  if (params.distance && params.distance.length) {
    queryParams.delete(C.PARAM_NAME_DISTANCE)
    params.distance.forEach((segmentDistance) => {
      queryParams.append(C.PARAM_NAME_DISTANCE, segmentDistance)
    })
  }
  if (params.time && params.time.length) {
    queryParams.delete(C.PARAM_NAME_TIME)
    params.time.forEach((segmentTime) => {
      const {hours, minutes, seconds} = secondsToTimeUnits(segmentTime)
      queryParams.append(C.PARAM_NAME_TIME, `${hours}h${minutes}m${seconds}s`)
    })
  }
  const queryParamsString = queryParams.toString()
  if (queryParamsString) {
    // URL is considered the source of truth for state, therefore state is not stored separately
    window.history.replaceState(null, '', `?${queryParamsString}`)
  }
}

function createSegmentsFromQueryParams() {
  let {distance: distances, time: times} = getUrlParams()
  distances = distances.length ? distances : [0]
  times = times.length ? times : [0]

  return distances.map((distance, index) => {
    const time = times[index] ?? 0
    const pace = distance && time ? time / distance : 6 * 60
    const lock = ''

    return {
      id: uniqid.time(),
      distance,
      time,
      pace,
      lock,
    }
  })
}

function setQueryParamsFromSegments(segments) {
  const params = {distance: [], time: []}
  segments.forEach((segment) => {
    if (segment.distance && segment.time) {
      params.distance.push(segment.distance)
      params.time.push(segment.time)
    }
  })

  setQueryParams(params)
}

function AddSegmentButton({onClickAdd}) {
  return (
      <button className="cursor-pointer"
              data-testid="segments-add"
              onClick={onClickAdd}><FaPlusCircle/></button>
  )
}

function DeleteSegmentButton({onClickDelete}) {
  return (
      <button className="cursor-pointer"
              data-testid="segments-delete"
              onClick={onClickDelete}><FaMinusCircle/></button>
  )
}

function FormView() {
  function onChangeUnit(unit) {
    dispatch({type: 'changed_unit', unit})
  }

  function onChangeDistance(id, distance) {
    dispatch({type: 'changed_distance', id, distance})
  }

  function onChangePace(id, pace) {
    dispatch({type: 'changed_pace', id, pace})
  }

  function onChangeTime(id, time) {
    dispatch({type: 'changed_time', id, time})
  }

  function onClickLock(id, lock) {
    dispatch({type: 'changed_lock', id, lock: lock.toLowerCase()})
  }

  function onClickAdd() {
    dispatch({type: 'added', id: uniqid.time()})
  }

  function onClickDelete(id) {
    dispatch({type: 'deleted', id})
  }

  function onReset({distance, pace, time, unit}) {
    if (!distance) {
      return
    }

    dispatch({
      type: 'resetted',
      distance,
      pace,
      time,
      unit,
      contextUnit,
    })
  }

  const {unit: contextUnit} = useContext(SettingsContext)
  const [segments, dispatch] = useReducer(segmentsReducer, createSegmentsFromQueryParams())
  useEffect(() => setQueryParamsFromSegments(segments), [segments])

  const totalDistance = segments.reduce((distance, segment) => distance + segment.distance, 0)
  const showWelcomeMessage = totalDistance === 0

  return (
    <div className="py-8 px-4 sm:px-8 md:px-12 lg:px-16 xl:px-24 xl:mx-auto">
      <Toolbar onChangeUnit={onChangeUnit}/>

      {segments.map((segment, index) => {
        return (
            <section key={`form-row-${segment.id}`} data-testid={`form-row-${index}`}
                     className="relative bg-grey-lighter w-full mx-auto mb-6 px-6 py-4 rounded">
              {index === 0 ? null : (
                  <div className="absolute text-xl" style={{top: '-0.5rem', right: '-0.5rem'}}>
                    <DeleteSegmentButton onClickDelete={onClickDelete.bind(this, segment.id)}/>
                  </div>
              )}
              <FormRow
                  distance={segment.distance}
                  pace={segment.pace}
                  time={segment.time}
                  lock={segment.lock}
                  onChangeDistance={onChangeDistance.bind(this, segment.id)}
                  onChangePace={onChangePace.bind(this, segment.id)}
                  onChangeTime={onChangeTime.bind(this, segment.id)}
                  onClickLock={onClickLock.bind(this, segment.id)}
              />
            </section>
        )
      })}

      <div className="text-2xl text-center">
        <AddSegmentButton onClickAdd={onClickAdd}/>
      </div>

      {segments.length > 1 && <SegmentSplitsTable segments={segments}/>}
      {segments.length === 1 && <SplitsTable distance={segments[0].distance} pace={segments[0].pace}/>}

      {showWelcomeMessage ? <WelcomeView onChange={onReset}/> : null}
    </div>
  )
}

export default FormView
