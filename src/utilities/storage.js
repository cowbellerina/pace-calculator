export function fetchState() {
  try {
    const serializedState = window.localStorage.getItem('paceCalculator')
    if (serializedState === null) {
      return undefined
    }
    return JSON.parse(serializedState)
  } catch (error) {
    console.debug(`Error while fetching state from localStorage: "${error.message}"`)
  }
}

export function saveState(state) {
  try {
    const {settings = {}} = state
    const newState = {settings}
    const serializedState = JSON.stringify(newState)
    localStorage.setItem('paceCalculator', serializedState)
  } catch (error) {
    console.debug(`Error while saving state to localStorage: "${error.message}"`)
  }
}
