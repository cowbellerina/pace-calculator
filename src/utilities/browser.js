export function hasTouch() {
    const mediaQueryList = window.matchMedia('(any-pointer: coarse)')
    return mediaQueryList && mediaQueryList.matches
}
