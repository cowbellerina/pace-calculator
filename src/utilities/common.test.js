import {formatDistance, extractIntegers, extractDecimals} from './common'

describe('formatDistance', () => {
  it('parses string values', () => {
    expect(formatDistance('13.37')).toEqual(13.4)
  })
  it('returns numbers with one decimal', () => {
    expect(formatDistance(13)).toEqual(13.0)
    expect(formatDistance(13.0037)).toEqual(13.0)
    expect(formatDistance(13.037)).toEqual(13.0)
    expect(formatDistance(13.37)).toEqual(13.4)
  })
})

describe('extractIntegers', () => {
  it('parses string values', () => {
    expect(extractIntegers('13')).toEqual(13)
    expect(extractIntegers('13.7')).toEqual(13)
    expect(extractIntegers('13.37')).toEqual(13)
  })
  it('parses float values', () => {
    expect(extractIntegers(13.0037)).toEqual(13)
    expect(extractIntegers(13.037)).toEqual(13)
    expect(extractIntegers(13.37)).toEqual(13)
  })
})

describe('extractDecimals', () => {
  it('parses string values', () => {
    expect(extractDecimals('13')).toEqual(0)
    expect(extractDecimals('13.7')).toEqual(70)
    expect(extractDecimals('13.37')).toEqual(37)
  })
  it('returns the first two decimals', () => {
    expect(extractDecimals(13.0037)).toEqual(0)
    expect(extractDecimals(13.037)).toEqual(4)
    expect(extractDecimals(13.37)).toEqual(37)
  })
})
