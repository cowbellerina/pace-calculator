import {secondsToTimeUnits, timeUnitsToSeconds, addTimeUnitToTime, toHhmmss} from './time'

describe('secondsToTimeUnits', () => {
  it('works with 0 value', () => {
    expect(secondsToTimeUnits(0)).toEqual({hours: 0, minutes: 0, seconds: 0})
  })
  it('returns seconds', () => {
    expect(secondsToTimeUnits(1)).toEqual({hours: 0, minutes: 0, seconds: 1})
    expect(secondsToTimeUnits(59)).toEqual({hours: 0, minutes: 0, seconds: 59})
  })
  it('returns minutes', () => {
    expect(secondsToTimeUnits(60)).toEqual({hours: 0, minutes: 1, seconds: 0})
    expect(secondsToTimeUnits(3540)).toEqual({hours: 0, minutes: 59, seconds: 0})
  })
  it('returns hours', () => {
    expect(secondsToTimeUnits(3600)).toEqual({hours: 1, minutes: 0, seconds: 0})
  })
  it('returns all time components', () => {
    expect(secondsToTimeUnits(49033)).toEqual({hours: 13, minutes: 37, seconds: 13})
  })
})

describe('timeUnitsToSeconds', () => {
  it('works with empty value', () => {
    expect(timeUnitsToSeconds({})).toEqual(0)
  })
  it('returns seconds', () => {
    expect(timeUnitsToSeconds({seconds: 59})).toEqual(59)
  })
  it('returns minutes', () => {
    expect(timeUnitsToSeconds({minutes: 1})).toEqual(60)
  })
  it('returns hours', () => {
    expect(timeUnitsToSeconds({hours: 1})).toEqual(3600)
  })
  it('returns sums up all time components', () => {
    expect(timeUnitsToSeconds({hours: 13, minutes: 37, seconds: 13})).toEqual(49033)
  })
  it('supports negative time (lol)', () => {
    expect(timeUnitsToSeconds({hours: -13,minutes: -37,seconds:  -13})).toEqual(-49033)
  })
})

describe('addTimeUnitToTime', () => {
  it('throws if called with invalid unit', () => {
    expect(() => {addTimeUnitToTime()}).toThrow('Invalid unit')
  })
  it('works with 0 value', () => {
    expect(addTimeUnitToTime(0, 0, 'h')).toEqual(0)
    expect(addTimeUnitToTime(0, 0, 'm')).toEqual(0)
    expect(addTimeUnitToTime(0, 0, 's')).toEqual(0)
  })
  it('adds seconds to total time', () => {
    expect(addTimeUnitToTime(49020, 13, 's')).toEqual(49033)
  })
  it('adds minutes to total time', () => {
    expect(addTimeUnitToTime(46813, 37, 'm')).toEqual(49033)
    expect(addTimeUnitToTime(0, 61, 'm')).toEqual(3660)
  })
  it('adds hours to total time', () => {
    expect(addTimeUnitToTime(2233, 13, 'h')).toEqual(49033)
  })
})

describe('toHhmmss', () => {
  it('works with 0 value', () => {
    expect(toHhmmss(0)).toEqual('00:00:00')
  })
  it('pads numbers', () => {
    expect(toHhmmss(3661)).toEqual('01:01:01')
  })
  it('supports omitting time units', () => {
    expect(toHhmmss(49034, {hours: false})).toEqual('37:14')
    expect(toHhmmss(49034, {seconds: false})).toEqual('13:37')

    // Doesn't make any sense to drop only minutes or all components from the result,
    // but hey, it's an internal utility function :)
    expect(toHhmmss(49034, {minutes: false})).toEqual('13:14')
    expect(toHhmmss(49034, {hours: false, minutes: false, seconds: false})).toEqual('')
  })
})
