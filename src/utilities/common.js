export function formatDistance(distance) {
  return parseFloat(parseFloat(distance).toFixed(1))
}

export function extractIntegers(distance) {
  return parseInt(distance, 10)
}

export function extractDecimals(distance) {
  return parseInt(parseFloat(distance).toFixed(2).slice(-2), 10)
}
