import {padStart} from 'lodash'

export function secondsToTimeUnits(time = 0) {
  let hours = 0
  if (time >= 3600) {
    hours = Math.floor(time / 3600)
    time = time - hours * 3600
  }
  const minutes = Math.floor(time / 60)
  const seconds = Math.floor(time - minutes * 60)
  return {hours, minutes, seconds}
}

export function timeUnitsToSeconds({hours = 0, minutes = 0, seconds = 0}) {
  const hoursAsSeconds = hours * 60 * 60
  const minutesAsSeconds = minutes * 60
  return hoursAsSeconds + minutesAsSeconds + seconds
}

export function addTimeUnitToTime(totalTime = 0, timeUnit = 0, unit) {
  const {hours, minutes, seconds} = secondsToTimeUnits(totalTime)

  if (unit === 'h') {
    return timeUnitsToSeconds({hours: timeUnit, minutes, seconds})
  }

  if (unit === 'm') {
    if (timeUnit >= 60) {
      return timeUnitsToSeconds({minutes: timeUnit, seconds})
    }
    return timeUnitsToSeconds({hours, minutes: timeUnit, seconds})
  }

  if (unit === 's') {
    return timeUnitsToSeconds({hours, minutes, seconds: timeUnit})
  }

  throw new Error('Invalid unit')
}

export function toHhmmss(time = 0, options) {
  options = Object.assign({}, {hours: true, minutes: true, seconds: true}, options)

  const {hours, minutes, seconds} = secondsToTimeUnits(time)
  const output = []

  if (options.hours !== false) {
    output.push(padStart(`${hours}`, 2, '0'))
  }
  if (options.minutes !== false) {
    output.push(padStart(`${minutes}`, 2, '0'))
  }
  if (options.seconds !== false) {
    output.push(padStart(`${seconds}`, 2, '0'))
  }

  return output.join(':')
}
