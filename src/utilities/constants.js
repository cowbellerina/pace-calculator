const constants = {
  RATIO_KM_TO_MI: 1.609344,
  RATIO_MI_TO_KM: 0.6213712,

  PARAM_VALUE_KM:'km',
  PARAM_VALUE_MI: 'mi',

  PARAM_NAME_DISTANCE: 'distance',
  PARAM_NAME_PACE: 'pace',
  PARAM_NAME_TIME: 'time'
}

export default constants
