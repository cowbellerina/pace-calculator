import {formatDistance} from './common'

export function getSplitsWithinDistance(distance = 0, split = 1) {
    if (distance <= 0 || split <= 0) {
        return []
    }

    if (distance <= split) {
        return [distance]
    }

    const totalSplits = distance / split
    const fullSplits = Math.floor(totalSplits)
    const hasRemainder = (totalSplits - fullSplits) !== 0

    const splits = []
    for (let i = 0; i < fullSplits; i++) {
        splits.push((i + 1) * split)
    }
    if (hasRemainder) {
        splits.push(distance)
    }

    return splits
}

export function getSplitsBySegments(segments, splitLength = 1) {
    let totalDistance = 0
    let totalTime = 0

    return segments.map((segment) => {
        const splits = getSplitsWithinDistance(segment.distance, splitLength)
        const rows = splits.map((splitDistance, index) => {
            const isLast = index === splits.length - 1
            const splitTime = splitDistance * segment.pace

            const split = {
                splitDistance,
                splitTime,
                totalDistance: formatDistance(splitDistance + totalDistance),
                totalTime: formatDistance(splitTime + totalTime),
            }

            if (isLast) {
                totalDistance = split.totalDistance
                totalTime = split.totalTime
            }

            return split
        })

        return {segment, splits: rows}
    })
}
