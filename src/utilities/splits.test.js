import {getSplitsWithinDistance, getSplitsBySegments} from './splits'

describe('getSplitsWithinDistance', () => {
  it('ignores zero values', () => {
    expect(getSplitsWithinDistance(0, 0)).toEqual([])
    expect(getSplitsWithinDistance(3, 0)).toEqual([])
    expect(getSplitsWithinDistance(0)).toEqual([])
  })
  it('returns full splits', () => {
    expect(getSplitsWithinDistance(3)).toEqual([1, 2, 3])
  })
  it('returns partial splits', () => {
    expect(getSplitsWithinDistance(3.13)).toEqual([1, 2, 3, 3.13])
  })
  it('supports distances shorter than split', () => {
    expect(getSplitsWithinDistance(0.3)).toEqual([0.3])
  })
  it('supports custom split length (integer)', () => {
    expect(getSplitsWithinDistance(11, 4)).toEqual([4, 8, 11])
  })
  it('supports custom split length (fraction)', () => {
    expect(getSplitsWithinDistance(1.1, 0.4)).toEqual([0.4, 0.8, 1.1])
  })
})

describe('getSplitsBySegments', () => {
  it('returns already crunched numbers by segment', () => {
    const segments =
      [
        {id: 'abc123def', distance: 2.1, time: 716.1, pace: 341},
        {id: 'bcd234efg', distance: 1, time: 300, pace: 300},
      ]

    const expected = [
      {
        segment: {id: 'abc123def', distance: 2.1, time: 716.1, pace: 341},
        splits: [
          {
            splitDistance: 1,
            splitTime: 341,
            totalDistance: 1,
            totalTime: 341,
          },
          {
            splitDistance: 2,
            splitTime: 682,
            totalDistance: 2,
            totalTime: 682,
          },
          {
            splitDistance: 2.1,
            splitTime: 716.1,
            totalDistance: 2.1,
            totalTime: 716.1,
          },
        ]
      },
      {
        segment: {id: 'bcd234efg', distance: 1, time: 300, pace: 300},
        splits: [
          {
            splitDistance: 1,
            splitTime: 300,
            totalDistance: 3.1,
            totalTime: 1016.1,
          }
        ]
      }
    ]

    const actual = getSplitsBySegments(segments)

    expect(actual).toEqual(expected)
  })
})
