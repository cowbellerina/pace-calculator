import React from 'react'
import { createRoot } from 'react-dom/client'

import {SettingsProvider} from './contexts/SettingsContext'
import FormView from './views/FormView/FormView'

import './styles/dist/index.css'

const container = document.getElementById('root')
const root = createRoot(container)
root.render(
    <SettingsProvider>
        <FormView />
    </SettingsProvider>
)
