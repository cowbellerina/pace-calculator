import C from '../utilities/constants'
import {formatDistance} from '../utilities/common'

const LOCK_PARAMETERS = [ C.PARAM_NAME_DISTANCE, C.PARAM_NAME_PACE, C.PARAM_NAME_TIME]
const LOCK_DEFAULT = ''
const SEGMENT_DEFAULT = {
    distance: 0,
    time: 0,
    pace: 6 * 60,
    lock: LOCK_DEFAULT,
}

export default function segmentsReducer(segments, action) {
    switch (action.type) {
        case 'added': {
            return [...segments, {...SEGMENT_DEFAULT, id: action.id}]
        }
        case 'deleted': {
            return segments.filter((segment) => segment.id !== action.id)
        }
        case 'resetted': {
            let distance, pace, time
            if (action.unit && action.unit !== action.contextUnit) {
                const multiplier = action.contextUnit === C.PARAM_VALUE_MI ? C.RATIO_KM_TO_MI : C.RATIO_MI_TO_KM
                distance = formatDistance(action.distance / multiplier)
            } else {
                distance = action.distance
            }

            if (action.time) {
                pace = Math.round(action.time / distance)
                time = action.time
            } else if (pace) {
                pace = action.contextUnit === C.PARAM_VALUE_MI ? Math.round(pace * C.RATIO_KM_TO_MI) : pace
                time = Math.round(pace * distance)
            }

            const segment = segments[0]
            return [{...segment, distance, time, pace}]
        }
        case 'changed_unit': {
            const multiplier = action.unit === C.PARAM_VALUE_MI ? C.RATIO_KM_TO_MI : C.RATIO_MI_TO_KM

            return segments.map((segment) => {
                if (segment.distance < 0.1) {
                    return segment
                }
                const distance = formatDistance(segment.distance / multiplier)
                const pace = Math.round(segment.time / distance)
                return {...segment, distance, pace}
            })
        }
        case 'changed_distance': {
            return segments.map((segment) => {
                if (segment.id !== action.id) {
                    return segment
                }
                if (segment.lock !== C.PARAM_NAME_TIME && segment.pace > 0) {
                    const time = Math.round(segment.pace * action.distance)
                    return {...segment, distance: action.distance, time}
                } else if (segment.lock !== C.PARAM_NAME_PACE && segment.time > 0) {
                    const pace = Math.round(segment.time / action.distance)
                    return {...segment, distance: action.distance, pace}
                }
                return {...segment, distance: action.distance}
            })
        }
        case 'changed_pace': {
            return segments.map((segment) => {
                if (segment.id !== action.id) {
                    return segment
                }
                if (segment.lock !== C.PARAM_NAME_TIME && segment.distance > 0) {
                    const time = Math.round(action.pace * segment.distance)
                    return {...segment, pace: action.pace, time}
                } else if (segment.lock !== C.PARAM_NAME_DISTANCE && segment.time > 0) {
                    const distance = formatDistance(segment.time / action.pace)
                    return {...segment, distance, pace: action.pace}
                }
                return {...segment, pace: action.pace}
            })
        }
        case 'changed_time': {
            return segments.map((segment) => {
                if (segment.id !== action.id) {
                    return segment
                }
                if (segment.lock !== C.PARAM_NAME_PACE && segment.distance > 0) {
                    const pace = Math.round(action.time / segment.distance)
                    return {...segment, pace, time: action.time}
                } else if (segment.lock !== C.PARAM_NAME_DISTANCE && segment.pace > 0) {
                    const distance = formatDistance(action.time / segment.pace)
                    return {...segment, distance, time: action.time}
                }
                return {...segment, time: action.time}
            })
        }
        case 'changed_lock': {
            return segments.map((segment) => {
                if (segment.id !== action.id || LOCK_PARAMETERS.indexOf(action.lock) === -1) {
                    return segment
                }
                if (segment.lock === action.lock) {
                    return {...segment, lock: LOCK_DEFAULT}
                }
                return {...segment, lock: action.lock}
            })
        }

        default:
            throw Error(`Unknown action: ${action.type}`)
    }
}
