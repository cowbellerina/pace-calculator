import segmentsReducer from './SegmentsReducer'

describe('when adding/deleting segments', () => {
    it('adds new segment with defaults', () => {
        const segments = [
            {id: 'abc123def', distance: 10, time: 3400, pace: 340, lock: ''},
        ]
        const expected = [
            {id: 'abc123def', distance: 10, time: 3400, pace: 340, lock: ''},
            {id: 'bcd234efg', distance: 0, time: 0, pace: 360, lock: ''},
        ]
        const actual = segmentsReducer(segments, {type: 'added', id: 'bcd234efg'})

        expect(actual).toEqual(expected)
    })
    it('deletes given segment', () => {
        const segments = [
            {id: 'abc123def', distance: 10, time: 3400, pace: 340, lock: ''},
            {id: 'bcd234efg', distance: 0, time: 0, pace: 360, lock: ''},
        ]
        const expected = [
            {id: 'bcd234efg', distance: 0, time: 0, pace: 360, lock: ''},
        ]
        const actual = segmentsReducer(segments, {type: 'deleted', id: 'abc123def'})

        expect(actual).toEqual(expected)
    })
})

describe('when changing unit', () => {
    it('converts distance and adjusts pace', () => {
        const segments = [
            {id: 'abc123def', distance: 10, time: 3400, pace: 340, lock: ''},
            {id: 'bcd234efg', distance: 5, time: 1800, pace: 360, lock: ''},
        ]
        const expected = [
            {id: 'abc123def', distance: 6.2, time: 3400, pace: 548, lock: ''},
            {id: 'bcd234efg', distance: 3.1, time: 1800, pace: 581, lock: ''},
        ]
        const actual = segmentsReducer(segments, {type: 'changed_unit', unit: 'mi'})

        expect(actual).toEqual(expected)
    })
})

describe('when changing distance', () => {
    it('only updates distance when time and pace are not set', () => {
        const segments = [
            {id: 'abc123def', distance: 10, time: 0, pace: 0, lock: ''},
        ]
        const expected = [
            {id: 'abc123def', distance: 13.37, time: 0, pace: 0, lock: ''},
        ]
        const actual = segmentsReducer(segments, {type: 'changed_distance', id: 'abc123def', distance: 13.37})

        expect(actual).toEqual(expected)
    })
    it('updates distance and time when time is not locked', () => {
        const segments = [
            {id: 'abc123def', distance: 10, time: 3600, pace: 360, lock: 'pace'},
        ]
        const expected = [
            {id: 'abc123def', distance: 13.37, time: 4813, pace: 360, lock: 'pace'},
        ]
        const actual = segmentsReducer(segments, {type: 'changed_distance', id: 'abc123def', distance: 13.37})

        expect(actual).toEqual(expected)
    })
    it('updates distance and pace when pace is not locked', () => {
        const segments = [
            {id: 'abc123def', distance: 10, time: 3600, pace: 360, lock: 'time'},
        ]
        const expected = [
            {id: 'abc123def', distance: 13.37, time: 3600, pace: 269, lock: 'time'},
        ]
        const actual = segmentsReducer(segments, {type: 'changed_distance', id: 'abc123def', distance: 13.37})

        expect(actual).toEqual(expected)
    })
})

describe('when changing pace', () => {
    it('only updates pace when distance and time are not set', () => {
        const segments = [
            {id: 'abc123def', distance: 0, time: 0, pace: 0, lock: ''},
        ]
        const expected = [
            {id: 'abc123def', distance: 0, time: 0, pace: 360, lock: ''},
        ]
        const actual = segmentsReducer(segments, {type: 'changed_pace', id: 'abc123def', pace: 360})

        expect(actual).toEqual(expected)
    })
    it('updates pace and time when time is not locked', () => {
        const segments = [
            {id: 'abc123def', distance: 10, time: 3600, pace: 360, lock: 'distance'},
        ]
        const expected = [
            {id: 'abc123def', distance: 10, time: 3400, pace: 340, lock: 'distance'},
        ]
        const actual = segmentsReducer(segments, {type: 'changed_pace', id: 'abc123def', pace: 340})

        expect(actual).toEqual(expected)
    })
    it('updates pace and distance when distance is not locked', () => {
        const segments = [
            {id: 'abc123def', distance: 10, time: 3600, pace: 360, lock: 'time'},
        ]
        const expected = [
            {id: 'abc123def', distance: 10.6, time: 3600, pace: 340, lock: 'time'},
        ]
        const actual = segmentsReducer(segments, {type: 'changed_pace', id: 'abc123def', pace: 340})

        expect(actual).toEqual(expected)
    })
})

describe('when changing time', () => {
    it('only updates time when distance and pace are not set', () => {
        const segments = [
            {id: 'abc123def', distance: 0, time: 0, pace: 0, lock: ''},
        ]
        const expected = [
            {id: 'abc123def', distance: 0, time: 3600, pace: 0, lock: ''},
        ]
        const actual = segmentsReducer(segments, {type: 'changed_time', id: 'abc123def', time: 3600})

        expect(actual).toEqual(expected)
    })
    it('updates time and distance when distance is not locked', () => {
        const segments = [
            {id: 'abc123def', distance: 10, time: 3600, pace: 360, lock: 'pace'},
        ]
        const expected = [
            {id: 'abc123def', distance: 8.3, time: 3000, pace: 360, lock: 'pace'},
        ]
        const actual = segmentsReducer(segments, {type: 'changed_time', id: 'abc123def', time: 3000})

        expect(actual).toEqual(expected)
    })
    it('updates time and pace when pace is not locked', () => {
        const segments = [
            {id: 'abc123def', distance: 10, time: 3600, pace: 360, lock: 'distance'},
        ]
        const expected = [
            {id: 'abc123def', distance: 10, time: 3000, pace: 300, lock: 'distance'},
        ]
        const actual = segmentsReducer(segments, {type: 'changed_time', id: 'abc123def', time: 3000})

        expect(actual).toEqual(expected)
    })
})

describe('when locking a parameter', () => {
    it('assigns the lock to given segment', () => {
        const segments = [
            {id: 'abc123def', lock: ''},
            {id: 'bcd234efg', lock: 'pace'},
        ]
        const expected = [
            {id: 'abc123def', lock: 'time'},
            {id: 'bcd234efg', lock: 'pace'},
        ]
        const actual = segmentsReducer(segments, {type: 'changed_lock', id: 'abc123def', lock: 'time'})

        expect(actual).toEqual(expected)
    })
    it('removes the assigned lock from given segment', () => {
        const segments = [
            {id: 'abc123def', lock: ''},
            {id: 'bcd234efg', lock: 'pace'},
        ]
        const expected = [
            {id: 'abc123def', lock: ''},
            {id: 'bcd234efg', lock: ''},
        ]
        const actual = segmentsReducer(segments, {type: 'changed_lock', id: 'bcd234efg', lock: 'pace'})

        expect(actual).toEqual(expected)
    })
    it('ignores incorrect lock parameters', () => {
        const segments = [
            {id: 'abc123def', lock: 'time'},
        ]
        const expected = [
            {id: 'abc123def', lock: 'time'},
        ]
        const actual = segmentsReducer(segments, {type: 'changed_lock', id: 'abc123def', lock: 'foobar'})

        expect(actual).toEqual(expected)
    })
})

describe('when resetting', () => {
    it('resets the state with given values', () => {
        const segments = [
            {id: 'abc123def', distance: 10, time: 3400, pace: 340, lock: 'time'},
            {id: 'bcd234efg', distance: 5, time: 1800, pace: 360, lock: 'pace'},
        ]
        const expected = [
            {id: 'abc123def', distance: 21.1, time: 7200, pace: 341, lock: 'time'},
        ]
        const actual = segmentsReducer(segments, {type: 'resetted', distance: 21.1, time: 7200, unit: 'km', contextUnit: 'km'})

        expect(actual).toEqual(expected)
    })
    it('converts distance and adjusts pace when values are given in different unit', () => {
        const segments = [
            {id: 'abc123def', distance: 10, time: 600, pace: 360},
        ]
        const expected = [
            {id: 'abc123def', distance: 13.1, time: 7200, pace: 550},
        ]
        const actual = segmentsReducer(segments, {type: 'resetted', distance: 21.1, time: 7200, unit: 'km', contextUnit: 'mi'})

        expect(actual).toEqual(expected)
    })
})
