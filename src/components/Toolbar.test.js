import React from 'react'
import {render, fireEvent, screen} from '@testing-library/react'
import Toolbar from './Toolbar'
import {SettingsContext, defaultSettings} from '../contexts/SettingsContext'

it('renders the component', () => {
  render(<Toolbar/>)
  const list = screen.queryByTestId('unit')
  expect(list).toBeInTheDocument()
})

it('renders unit from context', () => {
  render(
    <SettingsContext.Provider value={{unit: 'mi'}}>
      <Toolbar/>
    </SettingsContext.Provider>
  )
  let list = screen.queryByTestId('unit')
  expect(list.value).toBe('mi')
})

it('calls SettingsContext onChange on change', () => {
  const onChange = jest.fn()
  render(
    <SettingsContext.Provider value={{...defaultSettings, onChangeSettings: onChange}}>
      <Toolbar/>
    </SettingsContext.Provider>
  )
  let list = screen.queryByTestId('unit')
  expect(list.value).toBe('km')
  fireEvent.change(list, {target: {value: 'mi'}})
  expect(onChange).toHaveBeenCalledTimes(1)
  expect(onChange).toHaveBeenCalledWith({unit: 'mi'})
})

it('calls upper onChange on change', () => {
  const onChange = jest.fn()
  render(<Toolbar onChangeUnit={onChange}/>)
  let list = screen.queryByTestId('unit')
  expect(list.value).toBe('km')
  fireEvent.change(list, {target: {value: 'mi'}})
  expect(onChange).toHaveBeenCalledTimes(1)
  expect(onChange).toHaveBeenCalledWith('mi')
})
