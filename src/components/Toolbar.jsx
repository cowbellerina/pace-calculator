import React from 'react'
import PropTypes from 'prop-types'

import C from '../utilities/constants'
import {SettingsConsumer} from '../contexts/SettingsContext'

const Toolbar = (props) => {
  return (
    <SettingsConsumer>
      {({unit, onChangeSettings}) => (
        <div className="text-right mb-2">
          <label htmlFor="unit" className="px-4 font-light">Unit of distance</label>
          <select id="unit"
                  defaultValue={unit}
                  onChange={(event) => {
                    const unit = event.currentTarget.value
                    if (typeof onChangeSettings === 'function') {
                      onChangeSettings({unit})
                    }
                    if (typeof props.onChangeUnit === 'function') {
                      props.onChangeUnit(unit)
                    }
                  }}
                  className="px-4 py-2 text-grey-darkest border border-grey-light rounded"
                  data-testid="unit"
          >
            <option name={C.PARAM_VALUE_KM} value={C.PARAM_VALUE_KM}>Kilometers</option>
            <option name={C.PARAM_VALUE_MI} value={C.PARAM_VALUE_MI}>Miles</option>
          </select>
        </div>
      )}
    </SettingsConsumer>
  )
}

Toolbar.propTypes = {
  onChangeUnit: PropTypes.func
}

export default Toolbar
